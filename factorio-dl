#!/bin/sh

# Inspired by the (complicated!) PKGBUILDs for factorio and
# factorio-experimental on https://aur.archlinux.org

# Ported to ~POSIX sh for fun (and *BSD compatibility)

: "${FACTORIO_LOGIN:="$(whoami)"}"
: "${FACTORIODL_CONFIG:="$HOME/.config/factorio-dl.conf"}"
: "${FACTORIO_TARGET:="linux64"}"

__usage() {
  cat << EOH
$0 [-di] [-o <output>] [-c config] [-l login -p password] [-t target] <version>
$0 -h

$0 will download the factorio game from the Factorio website, with no
interaction from the user.

-d
  enable debug (set -x)

-i
  interactive
  if enabled, we might require some human interaction; it is not the default

-t
  specify the target platform; output will be changed accordingly.
  Known values:
   - linux64
   - osx
   - win64
   - win64-manual
  By default: $FACTORIO_TARGET

-o output
  specify output file. By default: factorio_alpha_x64_<version>.tar.xz

-l login -p password
  specify login and password values to supply at https://factorio.com/login
  FACTORIO_LOGIN default value: $FACTORIO_LOGIN
  FACOTRIO_PASSWORD default value: <empty>

-c configfile
  specify the location of a file holding login and password.
  It is a shell script that will be sourced, make sure it's not doing anything
  bad.
  This file should contain 2 lines:
    FACTORIO_LOGIN="your login here"
    FACTORIO_PASSWORD="your password here"
    #FACTORIO_TARGET="your target"
  default location: $FACTORIODL_CONFIG
EOH
}

__cleanup() {
  [ -f "$cookie" ] && rm "$cookie"
}

trap __cleanup INT TERM

__die() {
  __cleanup
  echo "$1" >&2; exit "${2:-1}"
}

while getopts ":dhic:l:p:o:t:" _opt; do
  case "$_opt" in
    d) set -x                      ;;
    h) __usage; exit 0             ;;
    i) _interactive="1"            ;;
    t) FACTORIO_TARGET="$OPTARG"   ;;
    c) FACTORIODL_CONFIG="$OPTARG" ;;
    l) FACTORIO_LOGIN="$OPTARG"    ;;
    p) FACTORIO_PASSWORD="$OPTARG" ;;
    o) output_file="$OPTARG"       ;;
    *) __die "$(__usage)" 1        ;;
  esac
done

shift "$((OPTIND-1))"

# We strip the first argument of anything that aren't numbers and dots
# It's needed because makepkg(1) will pass 'protocol://version' as $1
# Humans invoking the script can still use version alone as $1
version="$(printf '%s\n' "$1" | grep -Eo '[0-9.]+')"
if [ -z "$version" ]; then
  __die "No version to download. See $0 -h" 2
fi

if [ -z "$output_file" ]; then
  case "$FACTORIO_TARGET" in
    linux64) output_file="factorio_alpha_x64_${version}.tar.xz" ;;
    win64-manual) output_file="Factorio_x64_${version}.zip"     ;;
    win64) output_file="Setup_Factorio_x64_${version}.exe"      ;;
    osx) output_file="factorio_alpha_${version}.dmg"            ;;
    *) __die "Unknown target platform: $FACTORIO_TARGET" 5      ;;
  esac
fi

FACTORIO_URL="https://www.factorio.com/get-download/${version}/alpha/${FACTORIO_TARGET}"

if [ -r "$FACTORIODL_CONFIG" ]; then
  . "$FACTORIODL_CONFIG"
elif [ -z "$FACTORIO_PASSWORD" ]; then
  if [ -z "$_interactive" ]; then
    __die "No password was set, aborting" 6
  else
    # We write a ~POSIX-compliant script; `read -s` doesn't work on e.g. FreeBSD
    stty -echo
    printf "Password: "
    read -r FACTORIO_PASSWORD
    stty echo
    printf '\n'
  fi
fi

cookie=$(mktemp)

csrf_token=$(
  curl --silent --fail \
       --cookie-jar "$cookie" \
       https://www.factorio.com/login |
       grep -Eo '(<.*id="csrf_token" .*value=")[^"]+' |
       grep -Eo '[^"]+$'
  )

if [ -z "$csrf_token" ]; then
  __die "Could not find the CSRF token. This script might be broken." 7
fi

if curl --dump-header - \
     --silent --fail \
     --cookie-jar "$cookie" \
     --cookie "$cookie" \
     https://www.factorio.com/login \
     --data-urlencode username_or_email="$FACTORIO_LOGIN" \
     --data-urlencode password="$FACTORIO_PASSWORD" \
     --data-urlencode csrf_token="$csrf_token" |
     grep -q '^Location: '; then
       :
else
  __die "Login failed" 8
fi

curl --retry 10 --retry-delay 3 \
     --fail --location \
     --cookie "${cookie}" \
     --continue-at - \
     --output "${output_file}.part" \
     "${FACTORIO_URL}" \
|| rm -f "${output_file}.part"

if [ ! -f "${output_file}.part" ]; then
  __die "Download failed" 9
fi

if [ -f "${output_file}.part" ]; then
  mv "${output_file}.part" "${output_file}"
fi

__cleanup
